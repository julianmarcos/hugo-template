# Hugo Template
## Setup
- Clone this repo
- Run `rm content/posts/my-first-post.md` thats an example post
- Run hugo new posts/Postname.md
- Edit file `content/posts/Postname.md` is in markdown
- Run `hugo server` for preview in 127.0.0.1:1313 and run `hugo` to build website
- Upload `static` folder to your web server/provider
## Why static sites
They are less vulnerable than dynamic and they normaly are most efficient in loanding the posts of a blog

This template is using the hugo theme terminal

[Use this template](https://gitea.com/repo/create?template_id=16338)